package com.course.Focus.rest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.course.Focus.model.Focus;
import com.course.Focus.model.WeatherOutput;
import com.course.Focus.repository.FocusRepository;
import com.weatherinpocket.model.WeatherPoJo;
@CrossOrigin
@RestController
public class FocusRestController {

	@Autowired
	private FocusRepository repository;
	
//	@RequestMapping("/weather")
//	public List<WeatherOutput> getAll()
//	{
//		System.out.println("Weather Rest API call");
//		String uri = "https://api.openweathermap.org/data/2.5/onecall?lat=51.485927&lon=0.24995&appid=988410a1f56e5ac4017bf4b5ca7d874c";
//		RestTemplate template = new RestTemplate();
//		WeatherPoJo result = template.getForObject(uri, WeatherPoJo.class);
//		
//		List<WeatherPoJo> weatherList = new ArrayList<>();
//		
//		weatherList.add(result);
//		
//		WeatherOutput output = new WeatherOutput();
//		
//		output.setLat(result.getLat());
//		output.setLon(result.getLon());
//		output.setDt(result.getCurrent().getDt());
//		output.setSunrise(result.getCurrent().getSunrise());
//		output.setSunset(result.getCurrent().getSunset());
//		output.setTemp(result.getCurrent().getTemp());
//		output.setFeelsLike(result.getCurrent().getFeelsLike());
//		output.setPressure(result.getCurrent().getPressure());
//		output.setHumidity(result.getCurrent().getHumidity());
//		output.setClouds(result.getCurrent().getClouds());
//		output.setWindSpeed(result.getCurrent().getWindSpeed());
//		
//		
//		List<WeatherOutput> outputList = new ArrayList<>();
//		outputList.add(output);
//		for(WeatherOutput woutput : outputList)
//		{
//			System.out.println(woutput);
//		}
//		
//		System.out.println("Weather Output"+output);
//		return outputList;
//		//return Arrays.asList(result);
//		//return repository.findAll();
//	}
	
	@RequestMapping("/courses")
	public List<Focus> getAll()
	{
		return repository.findAll();
	}
	
	@RequestMapping("/courses/test")
	public Map<String,String> getAllTestData()
	{
		Map<String,String> map = new HashMap<>();
		map.put("ApplicationName", "FocusApp");
		return map;
	}
	
	@RequestMapping("/course/{id}")
	public ResponseEntity<Focus> getById(@PathVariable Long id) throws Exception
	{
		Focus focus = repository.findById(id).orElseThrow(()->new Exception("No Course Found for the "+id));
		
		return ResponseEntity.ok().body(focus);
	}
	
	@RequestMapping(value = "/course", method = RequestMethod.POST)
	public List<Focus> save(@RequestBody Focus focus)
	{
		repository.save(focus);
		
		return repository.findAll();
	}
	
	@RequestMapping(value = "/course/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Focus> update(@PathVariable Long id,@RequestBody Focus focusNew) throws Exception
	{
		Focus focus = repository.findById(id).orElseThrow(()->new Exception("No Course Found for the "+id));
		
		if(focusNew.getName()!=null)
		focus.setName(focusNew.getName());
		
		if(focusNew.getFees()!=0)
		focus.setFees(focusNew.getFees());
		
		if(focusNew.getStudentId()!=0)
			focus.setStudentId(focusNew.getStudentId());
		
		Focus updated = repository.save(focus);
		return ResponseEntity.ok(updated);
		
	}
	
	@RequestMapping(value = "/course/{id}", method = RequestMethod.DELETE)
	public List<Focus> delete(@PathVariable Long id)
	{
		repository.deleteById(id);
		return repository.findAll();
	}
}
