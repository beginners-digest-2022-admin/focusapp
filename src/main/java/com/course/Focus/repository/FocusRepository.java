package com.course.Focus.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.course.Focus.model.Focus;

@Repository
public interface FocusRepository extends JpaRepository<Focus, Long> {

}
