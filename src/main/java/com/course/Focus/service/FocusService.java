package com.course.Focus.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.course.Focus.model.Focus;
import com.course.Focus.repository.FocusRepository;

@Service
public class FocusService {

	@Autowired
	private FocusRepository repository;
	
	public List<Focus> getAllCources()
	{
		return repository.findAll();
	}
	
	public Focus getCourseById(Long id) throws Exception
	{
		return repository.findById(id).orElseThrow(()->new Exception("Course not Found"+id));
	}
	
	public void createCourseDetails(Focus focus)
	{
		repository.save(focus);
	}
	
	public void deleteCourseById(Long id) throws Exception
	{
		Focus focus = repository.findById(id).orElseThrow(()->new Exception("Course not Found"+id));
		repository.deleteById(focus.getId());
	}
}
