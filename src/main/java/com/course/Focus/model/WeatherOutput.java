package com.course.Focus.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"lat",
"lon",
"dt",
"sunrise",
"sunset",
"temp",
"feels_like",
"pressure",
"humidity",
"clouds",
"wind_speed"
})
@Generated("jsonschema2pojo")
public class WeatherOutput {

@JsonProperty("lat")
private Float lat;
@JsonProperty("lon")
private Float lon;
@JsonProperty("dt")
private Integer dt;
@JsonProperty("sunrise")
private Integer sunrise;
@JsonProperty("sunset")
private Integer sunset;
@JsonProperty("temp")
private Float temp;
@JsonProperty("feels_like")
private Float feelsLike;
@JsonProperty("pressure")
private Integer pressure;
@JsonProperty("humidity")
private Integer humidity;
@JsonProperty("clouds")
private Integer clouds;
@JsonProperty("wind_speed")
private Float windSpeed;


public WeatherOutput() {
	super();
	// TODO Auto-generated constructor stub
}

public WeatherOutput(Float lat, Float lon, Integer dt, Integer sunrise, Integer sunset, Float temp, Float feelsLike,
		Integer pressure, Integer humidity, Integer clouds, Float windSpeed) {
	super();
	this.lat = lat;
	this.lon = lon;
	this.dt = dt;
	this.sunrise = sunrise;
	this.sunset = sunset;
	this.temp = temp;
	this.feelsLike = feelsLike;
	this.pressure = pressure;
	this.humidity = humidity;
	this.clouds = clouds;
	this.windSpeed = windSpeed;
}

public Float getLat() {
	return lat;
}

public void setLat(Float lat) {
	this.lat = lat;
}

public Float getLon() {
	return lon;
}

public void setLon(Float lon) {
	this.lon = lon;
}

public Integer getDt() {
	return dt;
}

public void setDt(Integer dt) {
	this.dt = dt;
}

public Integer getSunrise() {
	return sunrise;
}

public void setSunrise(Integer sunrise) {
	this.sunrise = sunrise;
}

public Integer getSunset() {
	return sunset;
}

public void setSunset(Integer sunset) {
	this.sunset = sunset;
}

public Float getTemp() {
	return temp;
}

public void setTemp(Float temp) {
	this.temp = temp;
}

public Float getFeelsLike() {
	return feelsLike;
}

public void setFeelsLike(Float feelsLike) {
	this.feelsLike = feelsLike;
}

public Integer getPressure() {
	return pressure;
}

public void setPressure(Integer pressure) {
	this.pressure = pressure;
}

public Integer getHumidity() {
	return humidity;
}

public void setHumidity(Integer humidity) {
	this.humidity = humidity;
}

public Integer getClouds() {
	return clouds;
}

public void setClouds(Integer clouds) {
	this.clouds = clouds;
}

public Float getWindSpeed() {
	return windSpeed;
}

public void setWindSpeed(Float windSpeed) {
	this.windSpeed = windSpeed;
}

@Override
public String toString() {
	return "WeatherOutput [lat=" + lat + ", lon=" + lon + ", dt=" + dt + ", sunrise=" + sunrise + ", sunset=" + sunset
			+ ", temp=" + temp + ", feelsLike=" + feelsLike + ", pressure=" + pressure + ", humidity=" + humidity
			+ ", clouds=" + clouds + ", windSpeed=" + windSpeed + "]";
}
}
