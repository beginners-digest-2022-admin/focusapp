package com.course.Focus.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Focus")
public class Focus {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String name;
	private int fees;
	private Long studentId;
	
	public Focus() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Focus(Long id, String name, int fees, Long studentId) {
		super();
		this.id = id;
		this.name = name;
		this.fees = fees;
		this.studentId = studentId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getFees() {
		return fees;
	}

	public void setFees(int fees) {
		this.fees = fees;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	@Override
	public String toString() {
		return "Focus [id=" + id + ", name=" + name + ", fees=" + fees + ", studentId=" + studentId + "]";
	}
}
