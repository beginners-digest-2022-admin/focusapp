package com.course.Focus.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.course.Focus.model.Focus;
import com.course.Focus.service.FocusService;

@Controller
public class FocusController {
	
	@Autowired
	private FocusService service;
	
	@GetMapping("/")
	public String viewHomePage(Model model)
	{
		List<Focus> courseList = service.getAllCources();
		model.addAttribute("courseList", courseList);
		return "index";
	}
	
	@GetMapping("/new")
	public String add(Model model)
	{
		model.addAttribute("Course", new Focus());
		
		return "new";
	}
	
	@RequestMapping(value = "/save",method = RequestMethod.POST)
	public String saveCourse(@ModelAttribute("Focus") Focus focus)
	{
		service.createCourseDetails(focus);
		return "redirect:/";
	}
	
	@RequestMapping("/edit/{id}")
	public ModelAndView update(@PathVariable Long id) throws Exception
	{
		ModelAndView mv = new ModelAndView("new");
		Focus focus = service.getCourseById(id);
		mv.addObject("courseObject", focus);
		return mv;
	}
	
	@RequestMapping("/delete/{id}")
	public String delete(@PathVariable Long id) throws Exception
	{
		service.deleteCourseById(id);
		
		return "redirect:/";
		
	}
	

}
